const { cpus } = require('../index');

describe('global tests', () => {
  it('must return a valid number of cpus', () => {
    expect.assertions(1);
    const count = cpus();
    expect(count).toBeGreaterThan(0);
  });
});
