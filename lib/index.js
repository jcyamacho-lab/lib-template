const os = require('os');

/**
 * returns the number of cpus
 * @example
 * const { cors } = require('lib-template');
 * console.log(cors());
 * @returns {number} number of cpus
 */
function cpus() {
  return os.cpus().length;
}

module.exports = {
  cpus
};
